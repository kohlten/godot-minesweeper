extends Control

const Small = 0
const Medium = 1
const Large = 2

var BoardScene = preload("res://scenes/board.tscn")

func _on_Quit_button_down():
	get_tree().quit()

func StartGame(mode):
	var scene = BoardScene.instance()
	get_tree().root.add_child(scene)
	var root = get_node("/root/")
	root.remove_child(self)
	scene.init(mode)
	queue_free()

func small():
	StartGame(Small)

func medium():
	StartGame(Medium)

func large():
	StartGame(Large)