extends Node2D

# Types
const None = 0
const Exploded = 1
const Mine = 2
const Number = 4

# State
const Blank = 0
const Shown = 1
const Flagged = 2

var Type = None
var State = Blank
var CurrentType = None
var CurrentState = Blank
var Neighbors = 0

var ExplodedImage = preload("res://assets/img/bomb_red.png")
var MineImage = preload("res://assets/img/bomb.png")
var BlankImage = preload("res://assets/img/blank.png")
var FlaggedImage = preload("res://assets/img/flag.png")
var NumberTable = {
	0: preload("res://assets/img/0board.png"),
	1: preload("res://assets/img/1board.png"),
	2: preload("res://assets/img/2board.png"),
	3: preload("res://assets/img/3board.png"),
	4: preload("res://assets/img/4board.png"),
	5: preload("res://assets/img/5board.png"),
	6: preload("res://assets/img/6board.png"),
	7: preload("res://assets/img/7board.png"),
	8: preload("res://assets/img/8board.png"),
}

func init(pos, size):
	self.translate(pos)
	$State.texture = BlankImage
	var currentSize = $State.texture.get_size()
	if currentSize != size:
		if currentSize > size:
			scale = Vector2(currentSize.x / size.x, currentSize.y / size.y) 
		else:
			scale = Vector2(size.x / currentSize.x, size.y / currentSize.y)

func _process(delta):
	if State == Shown:
		if CurrentType != Type:
			match Type:
				Exploded:
					$State.texture = ExplodedImage
				Mine:
					$State.texture = MineImage
				Number:
					$State.texture = NumberTable[Neighbors]
			CurrentType = Type
	else:
		if CurrentState != State:
			match State:
				Blank:
					$State.texture = BlankImage
				Flagged:
					$State.texture = FlaggedImage
			CurrentState = State

func SetType(type):
	Type = type

func SetNeighbors(neighbors):
	if neighbors > 8 or neighbors < 0:
		return
	Neighbors = neighbors

func SetState(state):
	State = state
