extends Node2D

const Small = 0
const Medium = 1
const Large = 2

const SmallSize = 9
const MediumSize = 16
const LargeSize = 24

const SmallMines = 10
const MediumMines = 35
const LargeMines = 85

var Mode
var Size
var Mines
var CellSize

var NumFlagged = 0
var FirstClick = true
var CanPlay = true

var Cells = Array()

var CellInstance = preload("res://scenes/cell.tscn")

func _init():
	randomize()

func init(mode):
	if mode < 0 or mode > 2:
		return -1
	Mode = mode
	if Mode == Small:
		Size = SmallSize
		Mines = SmallMines
	elif Mode == Medium:
		Size = MediumSize
		Mines = MediumMines
	elif Mode == Large:
		Size = LargeSize
		Mines = LargeMines
	CellSize = Vector2(OS.window_size.x / Size, OS.window_size.y / Size) 
	init_cells()

func init_cells():
	for i in range(Size):
		Cells.append(Array())
		for j in range(Size):
			var cell = CellInstance.instance() 
			var pos = Vector2(i * CellSize.x, j * CellSize.y) + (CellSize / 2)
			cell.init(pos, CellSize)
			Cells[i].append(cell)
			add_child(cell)

func init_mines(mouseCellPos):
	var mouseCells = Array()
	for i in range(mouseCellPos.x - 1, mouseCellPos.x + 2):
		for j in range(mouseCellPos.y - 1, mouseCellPos.y + 2):
			if i >= 0 and i < Size and j >= 0 and j < Size:
				mouseCells.append(Vector2(i, j))
	for i in range(Mines):
		var pos
		while true:
			pos = Vector2(randi() % Size, randi() % Size)
			var found = false
			for j in range(len(mouseCells)):
				if pos == mouseCells[j]:
					found = true
					break
			if found:
				continue
			if Cells[pos.x][pos.y].Type != Cells[pos.x][pos.y].Mine:
				break
		Cells[pos.x][pos.y].SetType(Cells[pos.x][pos.y].Mine)

func init_neighbors():
	for i in range(Size):
		for j in range(Size):
			var neighbors = count_neighbors(Vector2(i, j))
			Cells[i][j].SetNeighbors(neighbors)

func count_neighbors(location):
	# Counting mine neighbors in a 3x3 zone.
	# S Being the current cell and C being all the cells it searches
	# C C C
	# C S C
	# C C C
	var count = 0
	for i in range(location.x - 1, location.x + 2):
		for j in range(location.y - 1, location.y + 2):
			if i >= 0 and i < Size and j >= 0 and j < Size:
				if Vector2(i, j) != location && Cells[i][j].Type == Cells[i][j].Mine:
					count += 1
	return count

func set_number():
	for i in range(Size):
		for j in range(Size):
			if Cells[i][j].Type != Cells[i][j].Mine:
				Cells[i][j].SetType(Cells[i][j].Number)

func _input(event):
	if event is InputEventKey && event.scancode == KEY_ESCAPE && event.pressed:
		if !$WindowDialog.visible:
			$WindowDialog.visible = true
			$WindowDialog.popup_centered()
			CanPlay = false
		else:
			$WindowDialog.rect_position = Vector2(-1000, -1000)
			$WindowDialog.visible = false
			CanPlay = true
	if !CanPlay:
		return
	if event is InputEventMouseButton:
		if event.pressed:
			var cellPos = (event.position / CellSize).floor()
			if event.button_index == BUTTON_LEFT:
				if FirstClick:
					FirstClick = false
					init_mines(cellPos)
					init_neighbors()
					set_number()
				if Cells[cellPos.x][cellPos.y].Type == Cells[cellPos.x][cellPos.y].Mine:
					Cells[cellPos.x][cellPos.y].SetType(Cells[cellPos.x][cellPos.y].Exploded)
					EndGame()
					return
				ClearCell(cellPos)
			elif event.button_index == BUTTON_RIGHT:
				FlagCell(cellPos)
				print("flag at ", cellPos)

func ClearCell(pos):
	Cells[pos.x][pos.y].SetState(Cells[pos.x][pos.y].Shown)
	if Cells[pos.x][pos.y].Neighbors > 0:
		return
	for i in range(pos.x - 1, pos.x + 2):
		for j in range(pos.y - 1, pos.y + 2):
			if i >= 0 and i < Size and j >= 0 and j < Size:
				var newPos = Vector2(i, j)
				if newPos != pos && Cells[i][j].State != Cells[i][j].Shown:
					ClearCell(Vector2(i, j))

func FlagCell(pos):
	if Cells[pos.x][pos.y].State == Cells[pos.x][pos.y].Shown:
		return
	if Cells[pos.x][pos.y].State == Cells[pos.x][pos.y].Flagged:
		Cells[pos.x][pos.y].SetState(Cells[pos.x][pos.y].Blank)
		if Cells[pos.x][pos.y].Type == Cells[pos.x][pos.y].Mine:
			NumFlagged -= 1
	else:
		Cells[pos.x][pos.y].SetState(Cells[pos.x][pos.y].Flagged)
		if Cells[pos.x][pos.y].Type == Cells[pos.x][pos.y].Mine:
			NumFlagged += 1

func ClearAll():
	for i in range(Size):
		for j in range(Size):
			Cells[i][j].SetState(Cells[i][j].Shown)

func EndGame():
	CanPlay = false
	ClearAll()

func _process(delta):
	if NumFlagged == Mines:
		EndGame()

func _on_Accept_button_down():
	get_tree().change_scene("res://scenes/menu.tscn")
	queue_free()

func _on_Deny_button_down():
	$WindowDialog.rect_position = Vector2(-1000, -1000)
	$WindowDialog.visible = false
	CanPlay = true
